import React, { useState } from "react";
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup'
import UserApi from "../../api/UserApi";

const SignUp = (props) => {
  const [isOpenModal, setOpenModal] = useState(false);
  const [email, setEmail] = useState("");
  const [isDisabledResendEmail, setDisabledResendEmail] = useState(false);
  const resendEmail = async () => {
    setDisabledResendEmail(true);
    await UserApi.resendEmailToActivate(email);
    setDisabledResendEmail(false);
  }
  const redirectToLogin = () => {
    props.history.push("/auth/sign-in");
  }

  return (
    <React.Fragment>
      <Formik
        initialValues={
          {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: '',
            confirmPassword: ''
          }
        }
        validationSchema={Yup.object({
          firstName: Yup.string()
            .max(50, 'Must be 50 characters or less')
            .required('Required'),
          lastName: Yup.string()
            .max(50, 'Must be 50 characters or less')
            .required('Required'),
          username: Yup.string()
            .min(6, 'Must be betweem 6 and 50 characters')
            .max(50, 'Must be betweem 6 and 50 characters')
            .required('Required')
            .test('checkUniqueUsername', 'This username is already registered.', async username => {
              // call api
              const isExists = await UserApi.existsByUsername(username);
              console.log(isExists);
              return !isExists;
            }),
          email: Yup.string()
            .email('Invalid email address')
            .required('Required')
            .test('checkUniqueEmail', 'This email is already registered.', async email => {
              // call api
              const isExists = await UserApi.existsByEmail(email);
              console.log(isExists);
              return !isExists;
            }),
          password: Yup.string()
            .min(6, 'Must be betweem 6 and 50 characters')
            .max(50, 'Must be betweem 6 and 50 characters')
            .required('Required'),
          confirmPassword: Yup.string()
            .required('Required')
            .when("password", {
              is: val => (val && val.length > 0 ? true : false),
              then: Yup.string().oneOf(
                [Yup.ref("password")],
                "Both password need to be the same"
              )
            })
        })}
        onSubmit={
          async (values) => {
            try {
              // call api
              await UserApi.create(
                values.firstName,
                values.lastName,
                values.username,
                values.email,
                values.password
              );
              // message
              setEmail(values.email);
              setOpenModal(true);
            } catch (error) {
              // console.log(error);
              props.history.push("/auth/500");
            }
          }
        }
        validateOnChange={false}
        validateOnBlur={false}
      >
        {({ isSubmitting }) => (
          <Card>
            <CardBody>
              <div className="m-sm-4">
                <Form>
                  <FormGroup>
                    <FastField
                      label="First Name"
                      type="text"
                      bsSize="lg"
                      name="firstName"
                      placeholder="Enter your first name"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Last Name"
                      type="text"
                      bsSize="lg"
                      name="lastName"
                      placeholder="Enter your last name"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Username"
                      type="text"
                      bsSize="lg"
                      name="username"
                      placeholder="Enter your username"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Email"
                      type="email"
                      bsSize="lg"
                      name="email"
                      placeholder="Enter your email"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Password"
                      type="password"
                      bsSize="lg"
                      name="password"
                      placeholder="Enter your password"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Confirm Password"
                      type="password"
                      bsSize="lg"
                      name="confirmPassword"
                      placeholder="Enter your password again"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <div className="text-center mt-3">
                    <Button type="submit" color="primary" size="lg" disabled={isSubmitting}>
                      Sign up
                    </Button>
                  </div>
                </Form>
              </div>
            </CardBody>
          </Card>
        )}
      </Formik>

      <Modal
        isOpen={isOpenModal}
      >
        <ModalHeader>
          You need to confirm your account
        </ModalHeader>
        <ModalBody className="m-3">
          <p className="mb-0">
            We have send an email to <strong>{email}</strong>.
          </p>
          <p className="mb-0">
            Please check your email to activate the account.
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={() => setOpenModal(false)}>
            Close
          </Button>{" "}
          <Button color="primary" onClick={resendEmail} disabled={isDisabledResendEmail}>
            Resend
          </Button>
          <Button color="primary" onClick={redirectToLogin}>
            Login
          </Button>
        </ModalFooter>
      </Modal>
    </React.Fragment>
  )
};

export default SignUp;
