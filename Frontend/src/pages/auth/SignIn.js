import React, { useState } from "react";
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  CustomInput,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup'
import LoginApi from "../../api/LoginApi";
import storage from "../../storage/Storage";
import useNotification from "../../customHooks/useNotification";
import defaultAvatar from "../../assets/img/avatars/default.jpg";
import UserApi from "../../api/UserApi";
import { setUserInfo, setToken } from "../../redux/actions/userInfoActions"
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const SignIn = (props) => {
  const notify = useNotification({ type: "error" });
  const [isOpenModal, setOpenModal] = useState(false);
  const [email, setEmail] = useState("");
  const [isDisabledResendEmail, setDisabledResendEmail] = useState(false);
  const resendEmail = async () => {
    setDisabledResendEmail(true);
    await UserApi.resendEmailToActivate(email);
    setDisabledResendEmail(false);
  }
  // remember me
  const [checkedRememberMe, setCheckedRememberMe] = useState(storage.isRememberMe());

  return (
    <React.Fragment>
      <div className="text-center mt-4">
        <h2>Welcome to Group Management system</h2>
        <p className="lead">Sign in to your account to continue</p>
      </div>

      <Formik
        initialValues={
          {
            username: '',
            password: ''
          }
        }
        validationSchema={Yup.object({
          password: Yup.string()
            .min(6, 'Must be betweem 6 and 50 characters')
            .max(50, 'Must be betweem 6 and 50 characters')
            .required('Required')
        })}
        onSubmit={
          async (values) => {
            try {
              // call api
              const result = await LoginApi.login(values.username, values.password);
              console.log(result);
              if (result.token === null || result.token === undefined) {
                setEmail(result.email);
                setOpenModal(true);
              } else {
                storage.setRememberMe(checkedRememberMe);

                // save token and userInfo to storage
                storage.setToken(result.token);
                storage.setUserInfo(
                  result.userName,
                  result.email,
                  result.firstName,
                  result.lastName,
                  result.role,
                  result.status
                );
                // save token and userInfo to redux
                props.setToken(result.token);
                props.setUserInfo(
                  result.userName,
                  result.email,
                  result.firstName,
                  result.lastName,
                  result.role,
                  result.status
                );
                props.history.push("/group-management")
              }

            } catch (error) {
              if (error.status === 401) {
                notify("Login failed!", "Username or password incorrect");
              } else {
                props.history.push("/auth/500");
              }
            }
          }
        }
        validateOnChange={false}
        validateOnBlur={false}
      >
        {({ isSubmitting }) => (
          <Card>
            <CardBody>
              <div className="m-sm-4">
                <div className="text-center">
                  <img
                    src={defaultAvatar}
                    alt="Chris Wood"
                    className="img-fluid rounded-circle"
                    width="132"
                    height="132"
                  />
                </div>
                <Form>
                  <FormGroup>
                    <FastField
                      label="Username"
                      type="text"
                      bsSize="lg"
                      name="username"
                      placeholder="Enter your username"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Password"
                      type="password"
                      bsSize="lg"
                      name="password"
                      placeholder="Enter your password"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <div>
                    <CustomInput
                      type="checkbox"
                      id="rememberMe"
                      label="Remember me next time"
                      defaultChecked={checkedRememberMe}
                      onChange={() => setCheckedRememberMe(!checkedRememberMe)}
                    />
                  </div>
                  <div className="mt-3">
                    <Link to="/auth/reset-password">Forgot password?</Link><br/>
                    Don't have an account? <Link to="/auth/sign-up">Sign up</Link>
                  </div>
                  <div className="text-center mt-3">
                    <Button type="submit" color="primary" size="lg" disabled={isSubmitting}>
                      Sign in
                    </Button>
                  </div>
                </Form>
              </div>
            </CardBody>
          </Card>
        )}
      </Formik>
      <Modal
        isOpen={isOpenModal}
      >
        <ModalHeader>
          You need to confirm your account
        </ModalHeader>
        <ModalBody className="m-3">
          <p className="mb-0">
            Your account is not activated.
          </p>
          <p className="mb-0">
            Please check your email <strong>{email}</strong> to activate the account.
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={resendEmail} disabled={isDisabledResendEmail}>
            Resend
          </Button>{" "}
          <Button color="secondary" onClick={() => setOpenModal(false)}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </React.Fragment>
  );
}

export default connect(null, { setUserInfo, setToken })(SignIn);