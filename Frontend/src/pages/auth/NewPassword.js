import React from "react";
import {
  Button,
  Card,
  CardBody,
  FormGroup,
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup'
import UserApi from "../../api/UserApi";
import { useParams } from "react-router-dom";
import useNotification from "../../customHooks/useNotification";

const NewPassword = (props) => {
  const { token } = useParams();
  // validate token
  // if (!token)

  const redirectToLogin = () => {
    props.history.push("/auth/sign-in");
  }
  const notify = useNotification();

  return (
    <React.Fragment>
      <div className="text-center mt-4">
        <h1 className="h2">Reset password</h1>
        <p className="lead">Enter your new password.</p>
      </div>
      <Formik
        initialValues={
          {
            password: '',
            confirmPassword: ''
          }
        }
        validationSchema={Yup.object({
          password: Yup.string()
            .min(6, 'Must be betweem 6 and 50 characters')
            .max(50, 'Must be betweem 6 and 50 characters')
            .required('Required'),
          confirmPassword: Yup.string()
            .required('Required')
            .when("password", {
              is: val => (val && val.length > 0 ? true : false),
              then: Yup.string().oneOf(
                [Yup.ref("password")],
                "Both password need to be the same"
              )
            })
        })}
        onSubmit={
          async (values) => {
            try {
              // call api
              await UserApi.resetPassword(token, values.password);
              // message
              notify("Success", "Your password has been reset!");
              props.history.push("/auth/sign-in");
            } catch (error) {
              // console.log(error);
              redirectToLogin();
            }
          }
        }
      // validateOnChange={false}
      // validateOnBlur={false}
      >
        {({ isSubmitting }) => (
          <Card>
            <CardBody>
              <div className="m-sm-4">
                <Form>
                  <FormGroup>
                    <FastField
                      label="Password"
                      type="password"
                      bsSize="lg"
                      name="password"
                      placeholder="Enter your password"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <FormGroup>
                    <FastField
                      label="Confirm Password"
                      type="password"
                      bsSize="lg"
                      name="confirmPassword"
                      placeholder="Enter your password again"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <div className="text-center mt-3">
                    <Button type="submit" color="primary" size="lg" disabled={isSubmitting}>
                      Reset password
                    </Button>
                  </div>
                </Form>
              </div>
            </CardBody>
          </Card>
        )}
      </Formik>
    </React.Fragment>
  )
};

export default NewPassword;
