import React, { useState } from "react";
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup'
import UserApi from "../../api/UserApi";

const ResetPassword = (props) => {
  const [isOpenModal, setOpenModal] = useState(false);
  const [email, setEmail] = useState("");
  const [isDisabledResendEmail, setDisabledResendEmail] = useState(false);
  const resendEmail = async () => {
    setDisabledResendEmail(true);
    await UserApi.resendEmailToResetPassword(email);
    setDisabledResendEmail(false);
  }

  return (
    <React.Fragment>
      <div className="text-center mt-4">
        <h1 className="h2">Reset password</h1>
        <p className="lead">Enter your email to reset your password.</p>
      </div>
      <Formik
        initialValues={
          {
            email: ''
          }
        }
        validationSchema={Yup.object({
          email: Yup.string()
            .email('Invalid email address')
            .required('Required')
            .test('checkUniqueEmail', 'This email is not registered.', async email => {
              // call api
              const isExists = await UserApi.existsByEmail(email);
              console.log(isExists);
              return isExists;
            })
        })}
        onSubmit={
          async (values) => {
            try {
              // call api
              await UserApi.resetPasswordRequest(values.email);
              // message
              setEmail(values.email);
              setOpenModal(true);
            } catch (error) {
              // console.log(error);
              props.history.push("/auth/500");
            }
          }
        }
        validateOnChange={false}
        validateOnBlur={false}
      >
        {({ isSubmitting }) => (
          <Card>
            <CardBody>
              <div className="m-sm-4">
                <Form>
                  <FormGroup>
                    <FastField
                      label="Email"
                      type="email"
                      bsSize="lg"
                      name="email"
                      placeholder="Enter your email"
                      component={ReactstrapInput}
                    />
                  </FormGroup>
                  <div className="text-center mt-3">
                    <Button type="submit" color="primary" size="lg" disabled={isSubmitting}>
                      Reset password
                    </Button>
                  </div>
                </Form>
              </div>
            </CardBody>
          </Card>
        )}
      </Formik>

      <Modal
        isOpen={isOpenModal}
      >
        <ModalHeader>
          You need to confirm resetting your password
        </ModalHeader>
        <ModalBody className="m-3">
          <p className="mb-0">
            We have send an email to <strong>{email}</strong>.
          </p>
          <p className="mb-0">
            Please check your email to reset your password.
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={() => setOpenModal(false)}>
            Close
          </Button>{" "}
          <Button color="primary" onClick={resendEmail} disabled={isDisabledResendEmail}>
            Resend
          </Button>
        </ModalFooter>
      </Modal>
    </React.Fragment>
  )
};

export default ResetPassword;
