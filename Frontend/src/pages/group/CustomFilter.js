import React from 'react'
import {
  Button, Col, Label, Row,
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup'
import { selectFilter } from '../../redux/selectors/groupSelector';
import { connect } from 'react-redux';

const CustomFilter = (props) => {
  return (
    <Formik
      enableReinitialize 
      initialValues={
        {
          minTotalMember: props.filter.minTotalMember ?? '',
          maxTotalMember: props.filter.maxTotalMember ?? ''
        }
      }
      validationSchema={Yup.object({
        minTotalMember: Yup.number()
          .positive('Must be greater than 0')
          .integer('Must be an integer'),
        maxTotalMember: Yup.number()
          .positive('Must be greater than 0')
          .integer('Must be an integer')
      })}
      onSubmit={values => {
        props.onTotalMemberFilter(values.minTotalMember, values.maxTotalMember);
      }}
      validateOnChange={false}
      validateOnBlur={false}
    >
      <Form>
        {/* <fieldset className="border p-2">
                <legend className="w-auto">Filter</legend> */}
        <fieldset className="filter-border">
          <legend className="filter-border">Filter</legend>
          <Row style={{ alignItems: "center" }}>
            <Col lg="auto">
              <Label className="mb-0">Total Member:</Label>
            </Col>
            <Col lg="2">
              <FastField
                type="number"
                name="minTotalMember"
                placeholder="Min"
                component={ReactstrapInput}
              />
            </Col>{" - "}
            <Col lg="2">
              <FastField
                type="number"
                name="maxTotalMember"
                placeholder="Max"
                component={ReactstrapInput}
              />
            </Col>
            <Col>
              <Button type="submit">
                Filter
              </Button>
            </Col>
          </Row>
        </fieldset>
      </Form>
    </Formik>
  )
}

const mapGlobalStateToProps = state => ({
  filter: selectFilter(state)
});

export default connect(mapGlobalStateToProps)(CustomFilter);