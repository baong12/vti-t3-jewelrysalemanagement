import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  Col,
  Container,
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Label
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup'
import { connect } from "react-redux";

import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import filterFactory, { customFilter } from 'react-bootstrap-table2-filter';

import { selectGroups, selectPaging, selectSelectedRows } from "../../redux/selectors/groupSelector";
import { setAllGroups, updateSelectedRowsAction } from "../../redux/actions/groupActions";
import GroupApi from "../../api/GroupApi";
import CustomSearch from "./CustomSearch";
import * as Icon from "react-feather";
import CustomFilter from "./CustomFilter";
import useNotification from "../../customHooks/useNotification";

const GroupManagement = (props) => {
  const setGroupsStore = props.setAllGroups;
  const sizePerPage = props.paging.sizePerPage;
  let onTotalMemberFilter = null;

  useEffect(() => {
    const reload = async () => {
      const result = await GroupApi.getAll(1, sizePerPage);
      const groups = result.content;
      const totalSize = result.totalElements;
      setGroupsStore(groups, totalSize, 1);
    }
    reload();
  }, [setGroupsStore, sizePerPage]);

  const actionFormatter = (cell, row, rowIndex) => {
    return (
      <Icon.Edit2 size={20} className="align-middle mr-2" onClick={() => updateGroup(row.id)} />
    );
  };

  const handleTableChange = async (type, { page, sortField, sortOrder, searchText, filters }) => {
    if (!sortField || !sortOrder) {
      sortField = 'id';
      sortOrder = 'desc';
    }

    // filter
    const filterVal = filters && filters.totalMember && filters.totalMember.filterVal ? filters.totalMember.filterVal : null;
    const minTotalMember = filterVal && filterVal.minTotalMember ? filterVal.minTotalMember : null;
    const maxTotalMember = filterVal && filterVal.maxTotalMember ? filterVal.maxTotalMember : null;

    const result = await GroupApi.getAll(page, sizePerPage, sortField, sortOrder, searchText, minTotalMember, maxTotalMember);
    const groups = result.content;
    const totalSize = result.totalElements;
    setGroupsStore(groups, totalSize, page, minTotalMember, maxTotalMember, searchText);
  }

  // filter
  const [isVisibleFilter, setVisibleFilter] = useState(false);

  const handleChangeFilter = (minTotalMember, maxTotalMember) => {
    onTotalMemberFilter({
      minTotalMember,
      maxTotalMember
    });
  }

  const tableColumns = [
    {
      dataField: "id",
      text: "ID",
      sort: true
    },
    {
      dataField: "name",
      text: "Name",
      sort: true
    },
    {
      dataField: "totalMember",
      text: "Total Member",
      sort: true,
      filter: customFilter(),
      filterRenderer: (onFilter, column) => {
        onTotalMemberFilter = onFilter;
        return null;
      }
    },
    {
      dataField: "action",
      text: "",
      formatter: actionFormatter,
      headerStyle: { width: '100px' },
      align: () => {
        return 'center';
      },
    }
  ];

  // refresh form
  const refreshForm = () => {
    props.updateSelectedRowsAction([]);

    handleTableChange(null, {
      page: 1,
      sortField: null,
      sortOrder: null,
      searchText: null,
      filters: null,
      search: null
    });
  }

  // create
  const [isOpenCreateModal, setOpenCreateModal] = useState(false);
  // const [isDisabledSave, setDisabledSave] = useState(false);
  const notifySuccess = useNotification();
  const notifyError = useNotification({ type: "error" });

  // update
  const [groupUpdateInfo, setGroupUpdateInfo] = useState();
  const updateGroup = async (groupId) => {
    setOpenUpdateModal(true);
    const groupInfo = await GroupApi.getById(groupId);
    setGroupUpdateInfo(groupInfo);
  }
  const [isOpenUpdateModal, setOpenUpdateModal] = useState(false);

  // delete
  const handleSelect = (row, isSelect) => {
    let selected = props.selectedRows;

    if (isSelect) {
      selected = [...props.selectedRows, row.id]
    } else {
      selected = props.selectedRows.filter(x => x !== row.id)
    }

    props.updateSelectedRowsAction(selected);
  }

  const handleSelectAll = (isSelect, rows) => {
    let selected = props.selectedRows;

    const ids = rows.map(r => r.id);
    if (isSelect) {
      selected = ids
    } else {
      selected = []
    }

    props.updateSelectedRowsAction(selected);
  }

  const deleteGroup = async () => {
    if (props.selectedRows.length !== 0) {
      try {
        await GroupApi.deleteByIds(props.selectedRows);
        notifySuccess(
          "Delete Group",
          "Delete Group Successfully!");
        refreshForm();
      } catch (error) {
        console.log(error);
        // redirect page error server
        props.history.push("/auth/500");
      }
    } else {
      notifyError(
        "Delete Group",
        "You must select at least ONE group"
      );
    }
  }

  return (
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">Group Management</h1>
      <Row>
        <Col>
          <Card>
            <CardBody>
              <ToolkitProvider
                keyField="id"
                data={props.groups}
                columns={tableColumns}
                search
              >
                {
                  toolkitprops => (
                    <>
                      {isVisibleFilter &&
                        <CustomFilter onTotalMemberFilter={handleChangeFilter} />
                      }
                      <Row>
                        <Col lg="4">
                          <CustomSearch {...toolkitprops.searchProps} />
                        </Col>
                        <Col lg="8" className="d-flex align-items-center">
                          <div className="flex-grow-1">
                            <div className="float-right pull-right">
                              <Icon.Filter className="ml-2" size={24} onClick={() => setVisibleFilter(!isVisibleFilter)} />
                              <Icon.RefreshCcw className="ml-2" size={24} onClick={() => refreshForm()} />
                              <Icon.PlusCircle className="ml-2" size={24} onClick={() => setOpenCreateModal(true)} />
                              <Icon.Trash2 className="ml-2" size={24} onClick={deleteGroup} />
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <BootstrapTable
                        {...toolkitprops.baseProps}
                        remote
                        bootstrap4
                        striped
                        bordered
                        noDataIndication="No data."
                        pagination={paginationFactory({
                          page: props.paging.page,
                          sizePerPage: props.paging.sizePerPage,
                          totalSize: props.paging.totalSize,
                          hideSizePerPage: true
                        })}
                        onTableChange={handleTableChange}
                        filter={filterFactory()}
                        selectRow={{
                          mode: 'checkbox',
                          clickToSelect: true,
                          selected: props.selectedRows,
                          onSelect: handleSelect,
                          onSelectAll: handleSelectAll
                        }}
                      />
                    </>
                  )
                }
              </ToolkitProvider>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Modal isOpen={isOpenCreateModal}>
        <Formik
          enableReinitialize
          initialValues={
            {
              name: ''
            }
          }
          validationSchema={Yup.object({
            name: Yup.string()
              .min(6, 'Must be betweem 6 and 50 characters')
              .max(50, 'Must be betweem 6 and 50 characters')
              .required('Required')
              .test('checkUniqueGroupName', 'This name is already registered.', async name => {
                // call api
                const isExists = await GroupApi.existsByName(name);
                return !isExists;
              }),
          })}
          onSubmit={async values => {
            try {
              await GroupApi.create(values.name);
              notifySuccess("Create Group", `Created "${values.name}" group successfully`);
              setOpenCreateModal(false);
              // TODO: refreshTable();
              refreshForm();
            } catch (error) {
              notifyError("Something went wrong!", error);
              props.history.push("/auth/500");
            }
          }}
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({ isSubmitting }) => (
            <Form>
              <ModalHeader>Craete Group</ModalHeader>
              <ModalBody className="m-3">
                <Row style={{ alignItems: "center" }}>
                  <Col lg="auto">
                    <Label className="mb-0">Total Member:</Label>
                  </Col>
                  <Col>
                    <FastField
                      type="text"
                      bsSize="lg"
                      name="name"
                      placeholder="Enter group name"
                      component={ReactstrapInput}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter>
                <Button type="submit" color="primary" disabled={isSubmitting}>
                  Save
                </Button>{" "}
                <Button color="secondary" onClick={() => setOpenCreateModal(false)}>
                  Close
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>

      <Modal isOpen={isOpenUpdateModal}>
        <Formik
          enableReinitialize
          initialValues={
            {
              name: groupUpdateInfo && groupUpdateInfo.name ? groupUpdateInfo.name : '',
              totalMember: groupUpdateInfo && groupUpdateInfo.totalMember !== undefined && groupUpdateInfo.totalMember !== null ? groupUpdateInfo.totalMember : ''
            }
          }
          validationSchema={Yup.object({
            name: Yup.string()
              .min(6, 'Must be between 6 and 50 characters')
              .max(50, 'Must be between 6 and 50 characters')
              .required('Required')
              .test('checkUniqueName', 'This name is already registered.', async name => {
                if (name === groupUpdateInfo.name) {
                  return true;
                }
                // call api
                const isExists = await GroupApi.existsByName(name);
                return !isExists;
              }),
            totalMember: Yup.number()
              .min(0, 'Must be greater than or equal 0 and integer')
              .integer('Must be greater than or equal 0 and integer')
          })}
          onSubmit={async values => {
            try {
              await GroupApi.update(
                groupUpdateInfo.id,
                values.name,
                values.totalMember
              );
              // show notification
              notifySuccess(
                "Update Group",
                "Update Group Successfully!"
              );
              // close modal
              setOpenUpdateModal(false);
              // Refresh table
              refreshForm();
            } catch (error) {
              notifyError("Something went wrong!", error);
              // redirect page error server
              props.history.push("/auth/500");
            }
          }}
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({ isSubmitting }) => (
            <Form>
              <ModalHeader>Update Group</ModalHeader>
              <ModalBody className="m-3">
                <Row style={{ alignItems: "center" }}>
                  <Col lg="auto">
                    <label>Group Name:</label>
                  </Col>
                  <Col>
                    <FastField
                      type="text"
                      bsSize="lg"
                      name="name"
                      placeholder="Enter group name"
                      component={ReactstrapInput}
                    />
                  </Col>
                </Row>
                <Row style={{ alignItems: "center" }}>
                  <Col lg="auto">
                    <label>Total Member:</label>
                  </Col>
                  <Col>
                    <FastField
                      type="number"
                      bsSize="lg"
                      name="totalMember"
                      placeholder="Enter total member"
                      component={ReactstrapInput}
                    />
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter>
                <Button type="submit" color="primary" disabled={isSubmitting}>
                  Save
                </Button>{" "}
                <Button color="secondary" onClick={() => setOpenUpdateModal(false)}>
                  Close
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik >
      </Modal>
    </Container>
  );
};

const mapGlobalStateToProps = state => ({
  groups: selectGroups(state),
  paging: selectPaging(state),
  selectedRows: selectSelectedRows(state)
});

export default connect(mapGlobalStateToProps, { setAllGroups, updateSelectedRowsAction })(GroupManagement);