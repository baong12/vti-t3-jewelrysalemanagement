import React from 'react';
import {
  Button,
  InputGroupAddon,
} from "reactstrap";
import { FastField, Form, Formik } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import { selectSearch } from '../../redux/selectors/groupSelector';
import { connect } from 'react-redux';

const CustomSearch = (props) => {
  return (
    <Formik
      enableReinitialize
      initialValues={
        {
          search: props.search ?? ''
        }
      }
      onSubmit={values => {
        props.onSearch(values.search);
      }}
    >
      <Form className='d-flex align-items-center'>
        <div className='flex-grow-1 mr-2'>
        <FastField
          type="text"
          name="search"
          placeholder="Search for..."
          component={ReactstrapInput}
        />
        </div>
        <InputGroupAddon addonType="append" color="primary">
          <Button type="submit">Search</Button>
        </InputGroupAddon>
      </Form>
    </Formik>
  );
}

const mapGlobalStateToProps = state => ({
  search: selectSearch(state)
});

export default connect(mapGlobalStateToProps)(CustomSearch);