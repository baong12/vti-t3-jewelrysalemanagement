import * as types from "../constants";

const initialState = {
    groups: [],
    paging: {
      page: 1,
      sizePerPage: 10,
      totalSize: 0
    },
    filter: {
      minTotalMember: null,
      maxTotalMember: null
    },
    search: null,
    selectedRows: []
};

export default function reducer(state = initialState, actions) {
  switch (actions.type) {
    case types.ALL_GROUPS:
      return {
        ...state,
        groups: actions.payload.groups,
        paging: {
          ...state.paging,
          page: actions.payload.page,
          totalSize: actions.payload.totalSize
        },
        filter: {
          ...state.filter,
          minTotalMember: actions.payload.minTotalMember,
          maxTotalMember: actions.payload.maxTotalMember
        },
        search: actions.payload.search
      };
    case types.GET_LIST_GROUP_SELECTED_ROWS:
      return {
        ...state,
        selectedRows: actions.payload
      };
    default:
      return state;
  }
}
