import * as types from "../constants";

export function setAllGroups(groups, totalSize, page, minTotalMember, maxTotalMember, search) {
  return {
    type: types.ALL_GROUPS,
    payload: {
      groups,
      totalSize,
      page,
      minTotalMember,
      maxTotalMember,
      search
    }
  };
};

export function updateSelectedRowsAction(selectedRows) {
  return {
    type: types.GET_LIST_GROUP_SELECTED_ROWS,
    payload: selectedRows
  };
};