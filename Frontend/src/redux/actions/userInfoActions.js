import * as types from "../constants";

export function setUserInfo(username, email, firstName, lastName, role, status) {
  return {
    type: types.USER_LOGIN_INFO,
    payload: {
      userName: username,
      email: email,
      firstName: firstName,
      lastName: lastName,
      role: role,
      status: status
    }
  };
}

export function setToken(token) {
  return {
    type: types.TOKEN,
    payload: token
  };
}