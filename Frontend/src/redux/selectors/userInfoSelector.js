import { createSelector } from "@reduxjs/toolkit";

const userInfoSelector = (state) => state.userInfo;

const selectUserInfoSelector = createSelector(
    userInfoSelector,
    state => state.userInfo
);

const selectTokenSelector = createSelector(
    userInfoSelector,
    state => state.token
);

const selectFullNameSelector = createSelector(
    selectUserInfoSelector,
    state => state.firstName + " " + state.lastName
);

export const selectUserInfo = state => {
    return selectUserInfoSelector(state);
};

export const selectToken = state => {
    return selectTokenSelector(state);
};

export const selectFullName = state => {
    return selectFullNameSelector(state);
};