import { createSelector } from "@reduxjs/toolkit";

const groupSelector = (state) => state.group;

const selectGroupsSelector = createSelector(
    groupSelector,
    state => state.groups
);

const selectPagingSelector = createSelector(
    groupSelector,
    state => state.paging
);

const selectFilterSelector = createSelector(
    groupSelector,
    state => state.filter
);

const selectSearchSelector = createSelector(
    groupSelector,
    state => state.search
);

const selectSelectedRowsSelector = createSelector(
    groupSelector,
    state => state.selectedRows
);

export const selectGroups = state => {
    return selectGroupsSelector(state);
};

export const selectPaging = state => {
    return selectPagingSelector(state);
};

export const selectFilter = state => {
    return selectFilterSelector(state);
};

export const selectSearch = state => {
    return selectSearchSelector(state);
};

export const selectSelectedRows = (state) => {
    return selectSelectedRowsSelector(state);
};