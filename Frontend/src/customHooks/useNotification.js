import { toastr } from "react-redux-toastr";

const useNotification = (options = {}) => {
  const { type = "success", timeOut = 5000, position = "top-right" } = options;

  const _options = {
    timeOut: timeOut,
    showCloseButton: false,
    progressBar: false,
    position: position
  };

  const toastrInstance =
    type === "info"
      ? toastr.info
      : type === "warning"
        ? toastr.warning
        : type === "error"
          ? toastr.error
          : toastr.success;

  return (title, message) => {
    toastrInstance(title, message, _options);
  }
}

export default useNotification;