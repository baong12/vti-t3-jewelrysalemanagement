import Api from './Api';

const url = "/users";

const existsByEmail = (email) => {
    return Api.get(`${url}/email/${email}`);
};

const existsByUsername = (username) => {
    return Api.get(`${url}/userName/${username}`);
};

const create = (firstName, lastName, username, email, password) => {
    const body = {
        firstName: firstName,
        lastName: lastName,
        userName: username,
        email: email,
        password: password
    }
    return Api.post(url, body);
};

const resendEmailToActivate = (email) => {
    const params = {
        email: email
    }
    return Api.get(`${url}/userRegistrationConfirmRequest`, { params: params });
};

const resetPasswordRequest = (email) => {
    const params = {
        email: email
    }
    return Api.get(`${url}/resetPasswordRequest`, { params: params });
};

const resendEmailToResetPassword = (email) => {
    const params = {
        email: email
    }
    return Api.get(`${url}/resendResetPassword`, { params: params });
};

const resetPassword = (token, newPassword) => {
    const params = {
        token: token,
        newPassword: newPassword
    }
    return Api.get(`${url}/resetPassword`, { params: params });
};

const getProfile = () => {
    return Api.get(`${url}/profile`);
};

const updateProfile = (avatarUrl) => {
    const body = {
        avatarUrl: avatarUrl
    }
    return Api.put(`${url}/profile`, body);
};

const getUserById = (id) => {
    return Api.get(`${url}/${id}`);
}

// export
const UserApi = { existsByEmail, existsByUsername, create, resendEmailToActivate, resetPasswordRequest, resendEmailToResetPassword, resetPassword, getProfile, updateProfile, getUserById }
export default UserApi;

