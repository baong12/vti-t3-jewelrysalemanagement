import Api from './Api';

const login = (username, password) => {
  const params = {
    username: username,
    password: password
  }
  return Api.get(`/login`, { params: params });
}

const LoginApi = { login }
export default LoginApi;