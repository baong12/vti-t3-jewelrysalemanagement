import Api from './Api';

const url = "/groups";

const getAll = (page = 1, size = 10, sortField = 'id', sortOrder = 'desc', search = '', minTotalMember = null, maxTotalMember = null) => {
  const params = {
    page,
    size,
    sort: `${sortField},${sortOrder}`
  }

  if (search) {
    params.search = search;
  }

  if (minTotalMember) {
    params.minTotalMember = minTotalMember;
  }

  if (maxTotalMember) {
    params.maxTotalMember = maxTotalMember;
  }

  return Api.get(url, { params: params });
}

const existsByName = (name) => {
  return Api.get(`${url}/name/${name}`)
}

const create = (name) => {
  const body = { name };
  return Api.post(url, body);
}

const getById = (id) => {
  return Api.get(`${url}/${id}`);
};

const update = (id, name, totalMember) => {
  const body = {
    name,
    totalMember
  }
  return Api.put(`${url}/${id}`, body);
};

const deleteByIds = (ids) => {
  return Api.delete(`${url}/${ids.toString()}`);
};

const GroupApi = { getAll, existsByName, create, getById, update, deleteByIds }
export default GroupApi;