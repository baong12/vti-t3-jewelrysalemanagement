package com.vti.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "`customers`")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "customer_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customerId;

	@Column(name = "`account_id`")
//	@OneToOne
	@JoinColumn(name = "account_id", referencedColumnName = "account_id")
	private Account account;

	@Column(name = "avatar", length = 20)
	private int avatar;

	@Column(name = "address", length = 100, nullable = false)
	private int address;

	@Column(name = "purchases_time")
	private int purchasesTime;

	@Column(name = "`amount_purchased`")
	private double amountPurchased;

	@Column(name = "`sale_code`", length = 10)
	private String saleCode;

	@Column(name = "`status`", nullable = false)
	private short status = 0;

	@Column(name = "`card_code`", length = 15)
	private String cardCode;

	@Column(name = "`security_code`", length = 3)
	private String securityCode;
	
	@OneToMany(mappedBy = "customer")
	private List<Order> orders;
	
	@OneToMany(mappedBy = "customer")
	private List<Rating> ratings;


	public Customer() {
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public int getAvatar() {
		return avatar;
	}

	public void setAvatar(int avatar) {
		this.avatar = avatar;
	}

	public int getAddress() {
		return address;
	}

	public void setAddress(int address) {
		this.address = address;
	}

	public int getPurchasesTime() {
		return purchasesTime;
	}

	public void setPurchasesTime(int purchasesTime) {
		this.purchasesTime = purchasesTime;
	}

	public double getAmountPurchased() {
		return amountPurchased;
	}

	public void setAmountPurchased(double amountPurchased) {
		this.amountPurchased = amountPurchased;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
