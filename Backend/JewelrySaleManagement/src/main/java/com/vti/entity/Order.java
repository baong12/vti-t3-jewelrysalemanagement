package com.vti.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "`orders`")
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "order_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int orderId;

	@ManyToOne
	@JoinColumn(name = "`customer_id`")
	private Customer customer;

	@ManyToOne
	@JoinColumn(name = "`product_id`")
	private Product product;

	@Column(name = "`quantity`", nullable = false)
	private int quantity = 0;

	@Column(name = "`total_currency`", nullable = false)
	private double totalCurrency = 0;

	@Column(name = "`payment_methods`", nullable = false)
	private short paymentMethods = 1;

	@Column(name = "`note`", length = 500)
	private String note;

	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "shipped_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date shippedDate;

	@Column(name = "received_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedDate;

	@Column(name = "returned_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date returnedDate;

	@Column(name = "`status`", nullable = false)
	private short status = 0;
	
	public Order() {
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotalCurrency() {
		return totalCurrency;
	}

	public void setTotalCurrency(double totalCurrency) {
		this.totalCurrency = totalCurrency;
	}

	public short getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(short paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Date getReturnedDate() {
		return returnedDate;
	}

	public void setReturnedDate(Date returnedDate) {
		this.returnedDate = returnedDate;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
