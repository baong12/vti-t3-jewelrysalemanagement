package com.vti.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "`products`")
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "product_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productId;

	@Column(name = "`product_name`", length = 50, nullable = false, unique = true)
	private String productName;

	@Column(name = "`main_material`", length = 50, nullable = false)
	private String mainMaterial;

	@Column(name = "`price`", nullable = false)
	private double price;

	@Column(name = "`image`", length = 30)
	private String image;

	@Column(name = "`quantity`", nullable = false)
	private int quantity = 0;

	@Column(name = "`warehouse_quantity`", nullable = false)
	private int warehouseQuantity = 0;

	@Column(name = "`sold_quantity`", nullable = false)
	private int soldQuantity = 0;

	@Column(name = "`status`", nullable = false)
	private short status = 0;

	@Column(name = "`content`", length = 1000)
	private String content;

	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "sale_start_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date saleStartDate;
	
	@OneToMany(mappedBy = "product")
	private List<Order> orders;
	
	@OneToMany(mappedBy = "product")
	private List<Rating> ratings;
	
	public Product() {
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getMainMaterial() {
		return mainMaterial;
	}

	public void setMainMaterial(String mainMaterial) {
		this.mainMaterial = mainMaterial;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getWarehouseQuantity() {
		return warehouseQuantity;
	}

	public void setWarehouseQuantity(int warehouseQuantity) {
		this.warehouseQuantity = warehouseQuantity;
	}

	public int getSoldQuantity() {
		return soldQuantity;
	}

	public void setSoldQuantity(int soldQuantity) {
		this.soldQuantity = soldQuantity;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getSaleStartDate() {
		return saleStartDate;
	}

	public void setSaleStartDate(Date saleStartDate) {
		this.saleStartDate = saleStartDate;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
