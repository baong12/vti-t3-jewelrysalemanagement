package com.vti.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.vti.dto.ChangePublicProfileDTO;
import com.vti.entity.Account;

public interface IAccountService extends UserDetailsService {

	void createAccount(Account account);

	Account findAccountByEmail(String email);

	Account findAccountByUsername(String username);

	void activeAccount(String token);

	void sendConfirmUserRegistrationViaEmail(String email);

	boolean existsAccountByEmail(String email);

	boolean existsAccountByUsername(String username);

	void resetPasswordViaEmail(String email);

	void resetPassword(String token, String newPassword);

	void sendResetPasswordViaEmail(String email);

	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
	
	void changeAccountProfile(String username, ChangePublicProfileDTO dto);

}
