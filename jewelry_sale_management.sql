-- Drop the database if it already exists
DROP DATABASE IF EXISTS jewelry_sale_management;
-- Create database
CREATE DATABASE IF NOT EXISTS jewelry_sale_management;
USE jewelry_sale_management;

-- Create table Registration_User_Token
DROP TABLE IF EXISTS 	`Registration_Account_Token`;
CREATE TABLE IF NOT EXISTS `Registration_Account_Token` ( 	
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`token`	 		CHAR(36) NOT NULL UNIQUE,
	`account_id` 		SMALLINT UNSIGNED NOT NULL,
	`expiryDate` 	DATETIME NOT NULL
);

-- Create table Reset_Password_Token
DROP TABLE IF EXISTS 	`Reset_Password_Token`;
CREATE TABLE IF NOT EXISTS `Reset_Password_Token` ( 	
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`token`	 		CHAR(36) NOT NULL UNIQUE,
	`account_id` 		SMALLINT UNSIGNED NOT NULL,
	`expiryDate` 	DATETIME NOT NULL
);

-- Create table accounts
DROP TABLE IF EXISTS 	`accounts`;
CREATE TABLE IF NOT EXISTS `accounts` ( 	
	`account_id` 	INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`username`	 	VARCHAR(50) NOT NULL UNIQUE CHECK (LENGTH(`username`) >= 6 AND LENGTH(`username`) <= 50),
    `first_name` 	VARCHAR(50) CHAR SET utf8mb4 NOT NULL,
	`last_name` 	VARCHAR(50) CHAR SET utf8mb4 NOT NULL,
	`birthday` 		DATE NOT NULL,
	`email` 		VARCHAR(50) NOT NULL UNIQUE CHECK (LENGTH(`email`) >= 6 AND LENGTH(`email`) <= 50),
	`phone_number` 	VARCHAR(15) NOT NULL UNIQUE,
	`password` 		VARCHAR(100) NOT NULL,
    `role` 			ENUM('ADMIN','CUSTOMER') DEFAULT 'CUSTOMER',
	`created_date`	DATETIME DEFAULT NOW() NOT NULL,
	`modified_date`	DATETIME DEFAULT NOW() NOT NULL,
    `status`		TINYINT DEFAULT 0, -- 0: Not Active, 1: Active
    `avatarUrl`		VARCHAR(500)

);

INSERT INTO `accounts` 	(`username` 	,`first_name` 	,`last_name` 	,`birthday` 	,`email` 					,`phone_number` 	,`password`  														,`role`  	,`created_date` ,`modified_date`, `status`)
VALUES				('nguyenkimbao' 	,'Nguyen' 		,'Kim Bao' 		,'1996-01-01' 	,'nguyenkimbao@gmail.com'	,'0901234561' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'ADMIN' 	, '2022-05-28'	, '2022-05-28', 1),
					('nguyenphuhung' 	,'Nguyen' 		,'Phu Hung' 	,'1996-02-01' 	,'nguyenphuhung@gmail.com'	,'0301234561' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'ADMIN' 	, '2022-05-28'	, '2022-05-28', 1),
					('phamhoangloc' 	,'Pham' 		,'Hoang Loc' 	,'1990-03-01' 	,'phamhoangloc@gmail.com'	,'0201234561' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'ADMIN' 	, '2022-05-28'	, '2022-05-28', 1),
					('buitathieu' 		,'Bui' 			,'Tat Hieu' 	,'1997-04-01' 	,'buitathieu@gmail.com'		,'0701234561' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'ADMIN' 	, '2022-05-28'	, '2022-05-28', 1),
					('nguyenthituoi' 	,'Nguyen' 		,'Thi Tuoi' 	,'1996-07-01' 	,'nguyenthituoi@gmail.com'	,'0801234561' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'ADMIN' 	, '2022-05-28'	, '2022-05-28', 1),
					('nguyenvanan' 		,'Nguyen' 		,'Van An' 		,'1996-05-01' 	,'nguyenvanan@gmail.com'	,'0901234562' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'CUSTOMER' , '2022-05-28'	, '2022-05-28', 0),
					('nguyenduydong' 	,'Nguyen' 		,'Duy Dong' 	,'1995-02-01' 	,'nguyenduydong@gmail.com'	,'0901234563' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'CUSTOMER' , '2022-05-28'	, '2022-05-28', 0),
					('nguyenthithao' 	,'Nguyen' 		,'Thi Thao' 	,'1996-04-01' 	,'nguyenthithao@gmail.com'	,'0901234564' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'CUSTOMER' , '2022-05-28'	, '2022-05-28', 0),
					('nguyenduylinh' 	,'Nguyen' 		,'Duy Linh' 	,'1995-06-01' 	,'nguyenduylinh@gmail.com'	,'0901234565' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 	,'CUSTOMER' , '2022-05-28'	, '2022-05-28', 0),
					('nguyenlananh' 	,'Nguyen' 		,'Lan Anh' 	,'1995-06-01' 	,'nguyenlananh@gmail.com'	,'0901234566' 		,'$2a$10$W2neF9.6Agi6kAKVq8q3fec5dHW8KUA.b0VSIGdIZyUravfLpyIFi' 		,'CUSTOMER' , '2022-05-28'	, '2022-05-28', 0);

-- Create table customers
DROP TABLE IF EXISTS 	`customers`;
CREATE TABLE IF NOT EXISTS `customers` ( 	
	`customer_id` 		INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`account_id`		INT UNSIGNED NOT NULL UNIQUE,
    `avatar` 			VARCHAR(20),
	`address` 			VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
	`purchases_time` 	INT DEFAULT 0,
	`amount_purchased` 	DOUBLE,
	`sale_code` 		VARCHAR(10),
	`status` 			TINYINT,
    `card_code` 		VARCHAR(15),
	`security_code`		VARCHAR(3),
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON UPDATE CASCADE ON DELETE CASCADE
);
INSERT INTO `customers` (`account_id` 	,`avatar` 				,`address` 						,`purchases_time` 	,`amount_purchased` 	,`sale_code` 	,`status` 	,`card_code`  	,`security_code`)
VALUES 				(6 					,'20220506123512.jpg' 	,'số 1 - Thanh Xuân - Hà Nội' 	,1 					,1000000				,NULL 			,0 			, NULL			, NULL),
					(7 					,'20220506123513.jpg' 	,'số 2 - Thanh Xuân - Hà Nội' 	,1 					,5000000				,NULL 			,0 			, NULL			, NULL),
					(8 					,'20220506123514.jpg' 	,'số 3 - Thanh Xuân - Hà Nội' 	,1 					,500000					,NULL 			,0 			, NULL			, NULL),
					(9 					,'20220506123515.jpg' 	,'số 4 - Thanh Xuân - Hà Nội' 	,1 					,500000					,NULL 			,0 			, NULL			, NULL),
					(10 				,'20220506123516.jpg' 	,'số 5 - Thanh Xuân - Hà Nội' 	,1 					,5000000				,NULL 			,0 			, NULL			, NULL);

-- Create table products
DROP TABLE IF EXISTS 	`products`;
CREATE TABLE IF NOT EXISTS `products` ( 	
	`product_id` 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`product_name`				VARCHAR(50) CHAR SET utf8mb4 NOT NULL UNIQUE,
    `main_material` 			VARCHAR(50),
	`price` 					DOUBLE NOT NULL,
	`image` 					VARCHAR(30),
	`quantity` 					INT DEFAULT 0,
	`warehouse_quantity` 		INT DEFAULT 0,
	`sold_quantity` 			INT DEFAULT 0,
    `status` 					TINYINT,
	`content`					VARCHAR(1000) CHAR SET utf8mb4,
    `created_date`				DATE,
    `sale_start_date` 			DATE
);
INSERT INTO `products` (`product_name` 	,`main_material` 	,`price` 		 ,`image` 			,`quantity` 	,`warehouse_quantity` 	,`sold_quantity` 	,`status`  	,`content`									, `created_date` 	, `sale_start_date`)
VALUES 				('vòng tay vàng'	, 'vàng' 			,5000000 		 ,'0000000001.jpg' 	,10			,9						, 1 				,2 			, 'sản phầm bằng vàng sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('vòng tay bạc'		, 'bạc' 			,500000 		 ,'0000000002.jpg' 	,10			,9						, 1 				,2 			, 'sản phầm bằng bạc sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('vòng cổ vàng'		, 'vàng' 			,5000000 		 ,'0000000003.jpg' 	,10			,9						, 1 				,2 			, 'sản phầm bằng vàng sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('vòng cổ bạc'		, 'bạc' 			,1000000 		 ,'0000000004.jpg' 	,10			,9						, 1 				,2 			, 'sản phầm bằng bạc sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('nhẫn kim cương'	, 'kim cương' 		,50000000 		 ,'0000000005.jpg' 	,10			,10						, 10 				,2 			, 'sản phầm bằng kim cương sang trọng...' 		, '2022-04-01' 		, '2022-05-29'),
					('kiềng vàng'		, 'vàng' 			,10000000 		 ,'0000000006.jpg' 	,10			,10						, 10 				,2 			, 'sản phầm bằng vàng sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('lắc tay hoa mai'	, 'vàng' 			,8000000 		 ,'0000000007.jpg' 	,5			,5						, 5 				,0 			, 'sản phầm bằng vàng sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('nhẫn đính đá'		, 'phỉ thúy' 		,20000000 		 ,'0000000008.jpg' 	,10			,10						, 10 				,2 			, 'sản phầm bằng đá quý sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('vòng ngọc'		, 'ngọc xanh' 		,10000000 		 ,'0000000009.jpg' 	,10			,10						, 10 				,1 			, 'sản phầm bằng đá quý sang trọng...' 			, '2022-04-01' 		, '2022-05-29'),
					('lắc chân'			, 'bạc' 			,500000 		 ,'0000000010.jpg' 	,10			,9						, 1 				,0 			, 'sản phầm bằng bạc sang trọng...' 			, '2022-04-01' 		, '2022-05-29');

-- Create table orders
DROP TABLE IF EXISTS 	`orders`;
CREATE TABLE IF NOT EXISTS `orders` ( 	
	`order_id` 					INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`customer_id`				INT UNSIGNED NOT NULL,
    `product_id` 				INT UNSIGNED NOT NULL,
	`quantity` 					INT NOT NULL,
	`total_currency` 			DOUBLE NOT NULL,
	`payment_methods` 			TINYINT DEFAULT 1 NOT NULL,
	`note` 						VARCHAR(500) CHAR SET utf8mb4,
	`created_date` 				DATETIME,
    `shipped_date` 				DATETIME,
	`received_date`				DATETIME,
	`returned_date`				DATETIME,
	`status`					TINYINT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(product_id) ON UPDATE CASCADE ON DELETE CASCADE
);
INSERT INTO `orders` (`customer_id` 	,`product_id` 	,`quantity` ,`total_currency` 	,`payment_methods` 	,`note` 	,`created_date` 	,`shipped_date`  	,`received_date`, `returned_date`, `status`)
VALUES 				(1 					,4 				,1 			,1000000 			,1 					, NULL 		,'2022-05-30' 		,'2022-05-30' 		,'2022-05-31' 	, NULL  		 , 2),
					(2 					,1 				,1 			,5000000 			,1 					, NULL 		,'2022-05-30' 		,'2022-05-30' 		,'2022-05-31' 	, NULL  		 , 2),
					(3 					,2 				,1 			,500000 			,1 					, NULL 		,'2022-05-30' 		,'2022-05-30' 		,'2022-05-31' 	, NULL  		 , 2),
					(4 					,10				,1 			,500000 			,1 					, NULL 		,'2022-05-30' 		,'2022-05-30' 		,'2022-05-31' 	, NULL  		 , 2),
					(5					,3 				,1 			,5000000 			,1 					, NULL 		,'2022-05-30' 		,'2022-05-30' 		,'2022-05-31' 	, NULL  		 , 2);

-- Create table rating
DROP TABLE IF EXISTS 	`rating`;
CREATE TABLE IF NOT EXISTS `rating` ( 	
	`rating_id` 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`product_id`				INT UNSIGNED NOT NULL,
    `customer_id` 				INT UNSIGNED NOT NULL,
	`rate` 						TINYINT,
	`content` 					VARCHAR(500) CHAR SET utf8mb4,
	`created_date` 				DATE,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(product_id) ON UPDATE CASCADE ON DELETE CASCADE
);
INSERT INTO `rating` (`product_id` 	,`customer_id` 	,`rate` ,`content` 	,`created_date`)
VALUES 				(1 				, 2				,5 		, NULL 		, '2022-05-31'),
					(2 				, 3				,5 		, NULL 		, '2022-05-31'),
					(3 				, 5				,3 		, NULL 		, '2022-05-31'),
					(4 				, 1				,5 		, NULL 		, '2022-05-31');

-- Create table statistical_month
DROP TABLE IF EXISTS 	`statistical_month`;
CREATE TABLE IF NOT EXISTS `statistical_month` ( 	
	`statistical_id` 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`statistical_ym`				VARCHAR(6),
    `solded_currency` 				DOUBLE,
	`solded_product_quantity` 		INT,
	`returned_product_quantity` 	INT,
	`rating_lowest_product_id` 		INT UNSIGNED,
	`bought_most_customer_id` 		INT UNSIGNED,
	`account_id` 					INT UNSIGNED,
    FOREIGN KEY (rating_lowest_product_id) REFERENCES products(product_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (bought_most_customer_id) REFERENCES customers(customer_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON UPDATE CASCADE ON DELETE CASCADE
);
INSERT INTO `statistical_month` (`statistical_ym` 	,`solded_currency` 	,`solded_product_quantity` ,`returned_product_quantity` 	,`rating_lowest_product_id`, `bought_most_customer_id`, `account_id`)
VALUES 							('202205' 			,12000000 			,5 						   ,0 								,1 							, 1 						  , 1);